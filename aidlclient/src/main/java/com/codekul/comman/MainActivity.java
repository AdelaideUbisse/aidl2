package com.codekul.comman;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ConcurrentModificationException;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private IComman comman;
    private final ServiceConnection serviceCon = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            comman = IComman.Stub.asInterface(iBinder);
            

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnBind).setOnClickListener((view) -> {

            Intent intent = new Intent("com.codekul.service.aidl");
            bindService(convertImplicitToExplicitIntent(intent), serviceCon, BIND_AUTO_CREATE);

});


        findViewById(R.id.btnCalculate).setOnClickListener((view) -> {
            try {
                Log.i("@codekul", "Addition is - " + comman.calculate(10, 100));
            } catch (NullPointerException | RemoteException e) {
                e.printStackTrace();
            }
        });

    }

    public Intent convertImplicitToExplicitIntent(Intent implicitIntent) {
        PackageManager pm = getPackageManager();
        @SuppressLint("QueryPermissionsNeeded") List<ResolveInfo> resolveInfoList = pm.queryIntentServices(implicitIntent, 0);

        if (resolveInfoList == null || resolveInfoList.size() != 1) {
            return null;
        }

        ResolveInfo serviceInfo = resolveInfoList.get(0);
        ComponentName component = new ComponentName(serviceInfo.serviceInfo.packageName, serviceInfo.serviceInfo.name);
        Intent explicitIntent = new Intent(implicitIntent);
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

}
